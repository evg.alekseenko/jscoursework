import './style.scss'

function component() {
    let element = document.createElement('div');
  
    // Lodash, currently included via a script, is required for this line to work
    element.innerHTML = 'Hello JS ITEA +';
    element.classList.add('hello');
  
    return element;
  }
  
  document.body.appendChild(component());